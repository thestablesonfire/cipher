package caesar

import(
	"math"

	"cipher/alphabet"
)

/*
 * Returns the Caesar cipher of inputString shifted shift characters
 */
func Cipher(inputString string, shift int) string {
	return swapCharacters(inputString, shift, false)
}

/*
 * Returns the Caesar decipher of inputString shifted shift characters
 */
func Decipher(inputString string, shift int) string {
	return swapCharacters(inputString, shift, true)
}

/*
 * Swaps the characters in input string by shift +/- depending on decipher
 */
func swapCharacters(inputString string, shift int, decipher bool) string {
	var newString = []byte(inputString)

	for index, char := range inputString {
		var characterString = string(char)
		var location = alphabet.CharPositionInAlphabet(characterString)

		if location != -1 {
			var newLocation = getNewPositionFromShift(location, shift, decipher)

			newString[index] = alphabet.Alphabet[newLocation]
		}
	}

	return string(newString)
}

/**
 * Returns the new position from the initial position shifted shift times
 */
func getNewPositionFromShift(initialPosition int, shift int, decipher bool) int {
	var length = len(alphabet.Alphabet)
	var newPos int;

	if decipher {
		newPos = initialPosition - shift

		if newPos < 0 {
			newPos += length
		}
		
	} else {
		newPos = initialPosition + shift
	}

	return int(math.Abs(float64(newPos % length)))
}
