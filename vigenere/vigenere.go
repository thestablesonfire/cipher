package vigenere

import(
	"cipher/alphabet"
)

func Cipher(input string, key string) string {
	return string(swapCharacters(input, key, false))
}

func Decipher(input string, key string) string {
	return string(swapCharacters(input, key, true))
}

func swapCharacters(input string, key string, decipher bool) []byte {
	var output = []byte(input)
	fullKey := buildKey(input, key)

	for i, char := range input {
		var inputLocation = alphabet.CharPositionInAlphabet(string(char))
		var keyLocation = alphabet.CharPositionInAlphabet(string(fullKey[i]))
		var newLocation int;

		if decipher {
			newLocation = inputLocation - keyLocation

			if newLocation < 0 {
				newLocation += len(alphabet.Alphabet)
			}
		} else {
			newLocation = inputLocation + keyLocation
		}

		output[i] = alphabet.Alphabet[newLocation % len(alphabet.Alphabet)]
	}

	return output
}

func buildKey(inputString string, key string) []byte {
	var keyLen int = len(key)
	newKey := make([]byte, len(inputString))

	for i := 0; i < len(inputString); i++ {
		newKey[i] = key[i % keyLen]
	}

	return newKey
}
