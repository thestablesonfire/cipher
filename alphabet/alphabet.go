package alphabet

// Alphabet contains all the characters for encoding/decoding
var Alphabet = []byte{
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ', '!', '"', '#', '$', '%',
	'&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '`',
}

// CharPositionInAlphabet returns the position of char s in alphabet slice
func CharPositionInAlphabet(s string) (position int) {
	position = -1

	for index, char := range Alphabet {
		if string(char) == s {
			position = index

			return
		}
	}

	return
}
