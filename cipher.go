package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"cipher/caesar"
	"cipher/vernam"
	"cipher/vigenere"
)

type caesarFn func(string, int) string
type vigenereFn func(string, string) string
type vernamFn func(string, string) string

func main() {
	cipherPtr, keyPtr, helpPtr, decipherPtr, shiftPtr := getFlags()
	checkForErrors(helpPtr)

	var cipherType = *cipherPtr
	var inputString string = strings.ToLower(flag.Arg(0))
	var outputString string

	switch cipherType {
	case "c", "caesar":
		shift := *shiftPtr
		var method caesarFn

		if *decipherPtr {
			method = caesar.Decipher
		} else {
			method = caesar.Cipher
		}

		outputString = method(inputString, shift)
	case "ver", "vernam":
		key := checkForKey(keyPtr, "Vernam")
		var method vernamFn

		if *decipherPtr {
			method = vernam.Decipher
		} else {
			method = vernam.Cipher
		}

		outputString = method(inputString, key)
	case "vig", "vigenere":
		key := checkForKey(keyPtr, "Vigenere")
		var method vigenereFn

		if *decipherPtr {
			method = vigenere.Decipher
		} else {
			method = vigenere.Cipher
		}

		outputString = method(inputString, key)
	default:
		fmt.Println("Please enter a valid cypher type. Caesar, Vigenere, or Vernam")
	}

	fmt.Println(outputString)
}

func getKey(keyPtr *string) string {
	return strings.ToLower(*keyPtr)
}

func checkForKey(keyPtr *string, cipherType string) string {
	if *keyPtr == "" {
		fmt.Println("Must provide a key for " + cipherType + " cipher")
		flag.PrintDefaults()
		os.Exit(1)
	}

	return getKey(keyPtr)
}

func checkForErrors(helpPtr *bool) {
	if *helpPtr {
		fmt.Println("Print help msg")
		os.Exit(1)
	} else if len(flag.Args()) < 1 {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func getFlags() (*string, *string, *bool, *bool, *int) {
	cipherPtr := flag.String("type", "caesar", "Cipher type")
	keyPtr := flag.String("key", "", "The key used to cipher/decipher the vigenere")
	helpPtr := flag.Bool("h", false, "Display the help dialog")
	decipherPtr := flag.Bool("d", false, "Whether you wish to decipher the passed string or not")
	shiftPtr := flag.Int("shift", 0, "The number of spaces to shift the input string")

	flag.Parse()

	return cipherPtr, keyPtr, helpPtr, decipherPtr, shiftPtr
}
