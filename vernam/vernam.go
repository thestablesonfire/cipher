package vernam

import (
	"fmt"
)

// http://www.cryptomuseum.com/crypto/vernam.htm

// Cipher Encodes input with key using the vernam cipher
func Cipher(input string, key string) string {
	return swapCharacters(input, key, false)
}

// Decipher doesn't work rn
func Decipher(input string, key string) string {
	fmt.Println("Decipher")
	return swapCharacters(input, key, true)
}

func swapCharacters(input string, key string, decipher bool) string {
	var output = []byte(input)
	fullKey := buildKey(input, key)

	for i := range input {
		output[i] = xOR(input[i], fullKey[i])
	}

	return string(output)
}

func xOR(a byte, b byte) byte {
	fmt.Println(a, b)
	return a ^ b
}

func buildKey(inputString string, key string) []byte {
	keyLen := len(key)
	newKey := make([]byte, len(inputString))

	for i := 0; i < len(inputString); i++ {
		newKey[i] = key[i%keyLen]
	}

	return newKey
}
